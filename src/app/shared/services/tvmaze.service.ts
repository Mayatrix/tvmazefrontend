import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http'
import { environment } from 'src/app/environments/environment';
import { map, Observable } from 'rxjs';
import { Show } from '../models/Show';
import { CastMember } from '../models/CastMember';

@Injectable({ providedIn: 'root'})
export class TvmazeService {
  BASEURL = environment.TVMAZE_API_URL
  SHOWSURL = "/shows"
  CASTURL = "/cast"
  

  constructor(private http: HttpClient) { }

  getShows(): Observable<Show[]| null> {
    return this.http
    .get<Show[]>(`${this.BASEURL + this.SHOWSURL}`, { observe: 'response'}).pipe(map((r: HttpResponse<Show[]>) => r.body))
  }

  getShowById(id: string): Observable<Show | null> {
    return this.http
    .get<Show>(`${this.BASEURL + this.SHOWSURL + '/' + id}`, { observe: 'response'}).pipe(map((r: HttpResponse<Show>) => r.body))
  }

  getCastByShowId(id: string): Observable<CastMember[] | null> {
    return this.http
    .get<CastMember[]>(`${this.BASEURL + this.SHOWSURL + '/' + id + this.CASTURL}`, { observe: 'response'}).pipe(map((r: HttpResponse<CastMember[]>) => r.body))
  }
}
