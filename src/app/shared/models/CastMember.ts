export interface CastMember {
    id: number;
    name: string;
    birthday: string;
    person: Person;
    character: Character;
}

export interface Person {
    name: string;
}

export interface Character {
    name: string;
}