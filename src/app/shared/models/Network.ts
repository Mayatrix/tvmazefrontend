export interface Network {
    id: number;
    name: string;
    country: Country
    officialSite: string;
}

export interface Country {
    name: string;
    code: string;
    timezone: string;
}