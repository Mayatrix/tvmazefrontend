import { CastMember } from "./CastMember";
import { External } from "./External";
import { Network } from "./Network";
import { Rating } from "./Rating";
import { Schedule } from "./Schedule";

export interface Show {
    id: number;
    url: string;
    name: string;
    type: string;
    language: string;
    genres: string[];
    status: string;
    runtime: number;
    averageRuntime: number;
    premiered: string;
    ended?: string;
    officialSite: string;
    schedule: Schedule;
    rating: Rating;
    weight: number;
    network: Network;
    webChannel?: string;
    dvdCountry?: string;
    externals: External;
    image: Image;
    summary: string;
    updated: number;
    _links: Link;
    cast: CastMember[]
}

export interface Image{
    medium: string;
    original: string;
}

export interface Link {
    self: {href: string;};
    previousepisode: {href: string;};
}