import { Component, Input} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CastMember } from 'src/app/shared/models/CastMember';
import { TvmazeService } from 'src/app/shared/services/tvmaze.service';
import { BehaviorSubject, lastValueFrom } from 'rxjs';
import { Show } from 'src/app/shared/models/Show';


@Component({
  selector: 'app-show-detail',
  templateUrl: './show-detail.component.html',
  styleUrls: ['./show-detail.component.scss']
})
export class ShowDetailComponent {
  shows: Show[] | null;
  specificShow: Show | null;
  castMembers: CastMember[] | null;

  async ngOnInit(): Promise<void> {
    this.loadData();
  }

  constructor(
    private route: ActivatedRoute,
    private tvMazeService: TvmazeService
  ) { }

  async loadData(): Promise<void>{
    const routeParams = this.route.snapshot.paramMap;
    const showId = Number(routeParams.get('showId'))
    this.shows = await lastValueFrom(this.tvMazeService.getShows())
    this.specificShow = this.shows![(showId - 1)]
  }

}
