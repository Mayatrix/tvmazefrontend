import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, lastValueFrom } from 'rxjs';
import { Show } from 'src/app/shared/models/Show';
import { TvmazeService } from 'src/app/shared/services/tvmaze.service';
import {PageEvent} from '@angular/material/paginator';

@Component({
  selector: 'app-shows',
  templateUrl: './shows.component.html',
  styleUrls: ['./shows.component.scss']
})

export class ShowsComponent implements OnInit {

  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [5, 10, 25];
  skip = 0;
  showNameSort: boolean = false;

  public currentPage: BehaviorSubject<{ skip: number }> = new BehaviorSubject(
    { skip: 0 }
  );

  public totalElementsPerPage = 10;

  ngOnInit(): void {
    this.loadData()
  }
  shows: Show[] | null;

  constructor(
    private tvmazeApiService: TvmazeService
  ) {}

  async loadData(): Promise<void> {
    this.shows = await lastValueFrom(this.tvmazeApiService.getShows())
  }

  onPageChange(event: any){
    this.skip = (event.pageIndex * event.pageSize)
  }

  sortShows(){
    this.showNameSort = !this.showNameSort
    this.showNameSort ? this.shows?.sort((a,b) => a.name.localeCompare(b.name)) : this.shows?.sort((a,b) => b.name.localeCompare(a.name)) 
  }


}
