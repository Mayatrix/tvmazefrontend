import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ShowDetailComponent } from './routes/shows/show-detail/show-detail.component';
import { ShowsComponent } from './routes/shows/shows.component';

const routes: Routes = [
  {
    path: "",
    component: ShowsComponent
  },
  {
    path: "home",
    component: ShowsComponent
  },
  {
    path: "shows/:showId",
    component: ShowDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
